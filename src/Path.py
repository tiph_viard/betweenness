
class Path():
    def __init__(self, init_links):
        self.nodes = {}
        self.duration = 0
        self.area = 0.0 
        self.length = 0
        self.path = [{"t": i[0], "u": i[1], "v":i[2]} for i in init_links]
        for i in init_links:
            self.nodes[i[1]] = i[0]
            self.nodes[i[2]] = i[0]
            self.length += 1
        self.duration = init_links[-1][0] - init_links[0][0] 

    def addLink(self, t,u,v):
        self.nodes[u] = t
        # self.nodes.add(u)
        self.nodes[v] = t
        # self.nodes.add(v)
        self.path.append({"t": t, "u": u, "v": v})
        self.duration = t - self.path[0]["t"]
        self.length += 1

    def links(self):
        return [ (l["t"], l["u"], l["v"]) for l in self.path] 

    def __repr__(self):
        return "->".join(\
                ["(" + str(i["t"]) + "," + str(i["u"]) + "," + str(i["v"]) + ")"\
                for i in self.path]
                )
