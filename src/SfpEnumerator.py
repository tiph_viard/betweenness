import sys
from collections import deque
from Path import *

class SortedDeque(deque):
    def __init__(self):
        super()
    def popleft(self):
        return deque.popleft(self)

class SfpEnumerator():

    def __init__(self, links, source, target):
        # S_init
        self.source = source
        self.target = target

        self.links = links
        self.S = SortedDeque()

        self.alpha = 0 #float(self.stream["T"]["alpha"])
        self.omega = 5 #float(self.stream["T"]["omega"])

        self.R = {}
        self.pathCount = 0
        self.paths = []
        # Read stream
        for u in self.links:
            for v in self.links[u]:
                t = v["t"]
                v = v["v"]
                if u == self.source or v == self.source:
                    self.S.append(Path([(t,u,v)])) 
        # Initialize result matrix with all possible times
        all_i = [self.alpha] + self.tau(self.source) + [self.omega]
        all_j = [self.alpha] + self.tau(self.target) + [self.omega]
        for (i,j) in zip(all_i, all_i[1:-1]):
            self.R[j] = {}

            for (x,y) in zip(all_j, all_j[1:]):
                if y > j:
                    self.R[j][y] = -1

    def tau(self, u):
        return [i["t"] for i in self.links[u] ]

    def getPath(self):
        s = [(x.duration, x.length) for x in self.S]
        min_path = s.index(min(s))
        self.S[0], self.S[min_path] = self.S[min_path], self.S[0]
        return self.S.popleft()

    def update_ij(self, p):
        # Should update p.area, too
        i2 = p.path[0]["t"]
        j1 = p.path[-1]["t"]

        # tau_u = [self.alpha] + self.tau(self.source) + [self.omega]
        # tau_v = [self.alpha] + self.tau(self.target) + [self.omega]

        for i in self.R:
            # c_i = (tau_u[tau_u.index(i)] - tau_u[tau_u.index(i) - 1]) 
            for j in self.R[i]:
                # c_j = (tau_v[tau_v.index(j)] - tau_v[tau_v.index(j) - 1])
                if i <= i2 and j > j1:
                    if self.R[i][j] == -1:
                        self.R[i][j] = [self.pathCount]
                        # Compute contribution to integral
                        # p.area += c_i * c_j
                    else:
                        if p.duration == self.paths[self.R[i][j][0]].duration:
                            if p.length < self.paths[self.R[i][j][0]].length:
                                self.R[i][j] = [self.pathCount]
                                # p.area += c_i * c_j
                            elif p.length == self.paths[self.R[i][j][0]].length:
                                self.R[i][j].append(self.pathCount)
                                # p.area += c_i * c_j
        # p.area = p.area / (self.omega - self.alpha)

    def enumerate(self):
        # Enumerate shortest fastest paths
        while len(self.S) > 0:
            e = self.getPath() # Select one current fastest path for examination
            # print(str(len(self.S)) + " " + str(len(set(self.S))))
            if self.target in e.nodes and self.source in e.nodes:
                if self.source == e.path[0]["u"]:
                    self.update_ij(e)
                    print(e)
                    self.paths.append((0,0))
                    self.paths[self.pathCount] = e # (e.duration, e.length)
                    self.pathCount += 1
            else:
                last_link = e.path[-1]
                for l in self.links[last_link["v"]]:
                    new_e = Path(e.links())
                    seen = (l["t"] == last_link["t"] and l["v"] == last_link["u"])
                    seen_vt = e.nodes.get(l["v"], None)
                    if not seen and l["t"] >= last_link["t"] and \
                            (seen_vt is None or l["t"] > seen_vt):
                        new_e.addLink(l["t"], last_link["v"], l["v"])
                        if not new_e in self.S:
                            self.S.append(new_e)
        return self.R, self.paths
