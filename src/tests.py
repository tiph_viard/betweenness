from SfpEnumerator import SfpEnumerator
import unittest as ut

TEST_DIR = "tests/"

class TestSfpEnumerator(ut.TestCase):
    
    def test_one_link(self):
        R_exp = SfpEnumerator(TEST_DIR + "one_link.json", "u", "v").enumerate()
        R = {
            2.0: {5.0: [0] },
        }
        self.assertEqual(R_exp, R)

    def test_two_links(self):
        R_exp = SfpEnumerator(TEST_DIR + "two_links.json", "u", "v").enumerate()
        R = {
                1.0: { 2.0: [0], 5.0: [0,1] },
                2.0: {5.0: [1]},
        }
        self.assertEqual(R_exp, R)

    def test_two_paths_different_durations(self):
        R_exp = SfpEnumerator(TEST_DIR + "two_paths_different_durations.json", "u", "v").enumerate()
        R = {
                1.0: { 2.0: -1, 4.0: [1], 5.0: [0] },
                4.0: {5.0: [0]}
        }
        self.assertEqual(R_exp, R)

    def test_fastest_is_not_foremost(self):
        R_exp = SfpEnumerator(TEST_DIR + "fastest_is_not_foremost.json", "u", "v").enumerate()
        R = {
                1.0: { 3.0: -1, 4.0: [0], 5.0: [0] }
        }
        self.assertEqual(R_exp, R)

    def test_unrelated_node(self):
        R_exp = SfpEnumerator(TEST_DIR + "unrelated_node.json", "u", "v").enumerate()
        R = {
                1.0: { 3.0: -1, 5.0: [0] },
                2.0: {3.0: -1, 5.0: -1}
        }
        self.assertEqual(R_exp, R)

    def test_two_paths_same_endpoints(self):
        R_exp = SfpEnumerator(TEST_DIR + "two_paths_same_endpoints.json", "u", "v").enumerate()
        R = {
                1.0: { 4.0: -1, 5.0: [0,1] }
        }
        self.assertEqual(R_exp, R)

    def test_two_paths_one_sfp_one_fp(self):
        R_exp = SfpEnumerator(TEST_DIR + "two_paths_one_sfp_one_fp.json", "u", "v").enumerate()
        R = {
                1.0: { 5.0: [0]}
        }
        self.assertEqual(R_exp, R)


if __name__ == '__main__':
    suite = ut.TestLoader().loadTestsFromTestCase(TestSfpEnumerator)
    ut.TextTestRunner(verbosity=2).run(suite)
