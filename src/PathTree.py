from collections import deque


class TimeNode():
    def __init__(self, _t, _v):
        self.t = _t
        self.v = _v

    def __eq__(self, _x):
        return self.t == _x.t and self.v == _x.v

    def __hash__(self):
        return hash(repr(self))

    def __str__(self):
        return "(" + str(self.t) + "," + str(self.v) + ")"
    def __repr__(self):
        return "(" + str(self.t) + "," + str(self.v) + ")"

class PathTree():
    
    def __init__(self, _root, _first):
        self.nodes = set()
        self.tree = dict()
        self.leaves = deque()
        self.root = _root

        self.nodes.add(_root.v)
        self.nodes.add(_first.v)

        self.tree[_root] = []
        self.tree[_root].append(_first)

        self.leaves.append(_first)

    def addLink(self, link):
        # Find (t,u)s in tree (BFS? Only keep leaves for consideration?)
        # Add (t,v) as neighbour of all (t,u) considered
        pass
