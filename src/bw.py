# Input : a file containing a t u v link stream, a pair of nodes
# Output : The shortest fastest paths from (i,u) to (j,v), for all i,j
from SfpEnumerator import SfpEnumerator
import sys
import matplotlib.pyplot as plt
import json
import collections
import itertools 
import logging
import configparser
from PathTree import PathTree, TimeNode


# Read configuration and configure logger
config = configparser.ConfigParser()
config.read('./config.ini')
logging.basicConfig(level=getattr(logging, config["DEFAULT"]["LogLevel"].upper()))
#print(logging.getLogger().getEffectiveLevel())
logger = logging.getLogger(__name__)
# The link stream
links = {}
# Its induced graph
graph = set()

try:
    stream = json.load(open(sys.argv[1]))
except:
    sys.exit()

# Read stream
for link in stream["E"]:
    t = float(link["t"])
    u = str(link["u"])
    v = str(link["v"].strip())
    
    uv = frozenset([u,v])

    if u in links:
        links[u].append(TimeNode(t, v))
    else:
        links[u] = [TimeNode(t, v)]

    if v in links:
        links[v].append(TimeNode(t, u))
    else:
        links[v] = [TimeNode(t, u)]

    graph.add(u)
    graph.add(v)

# deque of PathTrees
S = collections.deque()

# Start with first *link* rather than first (t,u), otherwise
# subsequent (t',u) will be added (or, have a better condition for
# candidate extension)
source = "u"
target = "v"

for i in links[source]:
    p = PathTree(TimeNode(i.t, source), TimeNode(i.t, i.v))
    S.append(p)

# Scheme: among all pathTrees, select the one that has min duration; i.e. P s.t. min(min(t:\exists (t,v) leaf of P) - root.t) 
R = []

while len(S) > 0:
    p = S.popleft()


    while len(p.leaves) > 0 and not all([x.v == target for x in p.leaves]):
        # Override to select min duration
        x = p.leaves.popleft()

        if not x in p.tree:
            p.tree[x] = []

        extended_time = False
        for i in links[x.v]:
            # Two types of "good candidates": same node, next time
            # or same time, different node
            if i.t == x.t:
                p.tree[x].append(i)
                if not i in p.tree:
                    p.tree[i] = []
            elif i.t > x.t and not extended_time:
                y = TimeNode(i.t, x.v)
                p.tree[x].append(y)
                extended_time = True
                if not y in p.tree:
                    p.tree[y] = []
     
        p.leaves = collections.deque([u for u in p.tree if len(p.tree[u]) == 0])
            
    R.append(p)
    logger.info(p.tree)

# Count sfps involving (t,v)
a = TimeNode(2.5, "y")
total_paths = 0
paths_involving_a = 0
for p in R:
    visited = set([p.root])
    Q = collections.deque()
    Q.append(p.root)
    old = p.root
    while len(Q) > 0:
        node = Q.pop()
        logger.info(node)
        for x in p.tree[node]:
            if not x in visited:
                Q.append(x)
                visited.add(x)
            if x in visited and not x == node:
                total_paths += 1
        if (node.v == a.v or old.v == a.v) and (old.t <= a.t and node.t >= a.t):
               logger.info("Ping-pong!")
               paths_involving_a += 1

        logger.info("Studying link :" + str(old) + " " + str(node) + " " + str(a))
        old = node
logger.info(total_paths)
logger.info(paths_involving_a)
