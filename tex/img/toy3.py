from streamfig import StreamFig

s = StreamFig.StreamFig(alpha=0, omega=3)
s.addColor("orange", "#ffa500")
s.addColor("red", "#ff0000")

s.addNode("a")
s.addNode("b")

s.addLink("a", "b", 1, 1)
s.addLink("a", "b", 2, 2)

s.addPath([(1, "a", "b")], start=1, end=1, color="orange", depth=48, width=3)
s.addPath([(2, "a", "b")], start=2, end=2, color=11, depth=48, width=3)

s.addTimeLine(ticks=1)
