from streamfig import StreamFig

s = StreamFig.StreamFig(alpha=0, omega=6)
s.addColor("orange", "#ffa500")
s.addColor("red", "#ff0000")

s.addNode("a")
s.addNode("b")
s.addNode("c")
s.addNode("d")

s.addLink("a", "b", 1, 1)
s.addLink("b", "c", 2, 2)
s.addLink("b", "d", 3, 3)
s.addLink("c", "d", 4, 4)
s.addLink("a", "d", 5, 5)

s.addPath([(1, "a", "b"), (3, "b", "d")], start=1, end=3, color="orange", depth=48, width=3)
s.addPath([(5, "a", "d")], start=5, end=5, color="red", depth=48, width=3)

s.addTimeLine(ticks=1)
