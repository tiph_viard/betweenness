from streamfig import StreamFig

s = StreamFig.StreamFig(alpha=0, omega=4)
s.addColor("orange", "#ffa500")
s.addColor("red", "#ff0000")

s.addNode("a")
s.addNode("b")
s.addNode("c")

s.addLink("a", "b", 1, 1)
s.addLink("a", "c", 2, 2)
s.addLink("b", "c", 3, 3)

s.addPath([(2, "a", "c")], start=2, end=2, color="orange", depth=48, width=3)

s.addTimeLine(ticks=1)
