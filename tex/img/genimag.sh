#!/bin/bash

for i in ./img/*.py;
do
    echo $i;
    fname=$(basename $i|cut -d. -f1)
    #echo $fname
    python $i > ./img/$fname.fig; fig2dev -Leps ./img/$fname.fig > ./img/$fname.eps
done
