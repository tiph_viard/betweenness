from streamfig import StreamFig

s = StreamFig.StreamFig(alpha=0, omega=2)

s.addNode("a")
s.addNode("b")

s.addLink("a", "b", 1, 1)

s.addPath([(1, "a", "b")], start=1, end=1, color=11, depth=48, width=3)

s.addTimeLine(ticks=1)
