from streamfig import StreamFig

s = StreamFig.StreamFig(alpha=0, omega=4)
s.addColor("orange", "#ffa500")
s.addColor("red", "#ff0000")

s.addNode("a")
s.addNode("b")
s.addNode("c")

s.addLink("a", "b", 1, 1)
s.addLink("b", "c", 2, 2)
s.addLink("a", "c", 3, 3)

s.addPath([(1, "a", "b"), (2, "b", "c")], start=1, end=2, color="orange", depth=48, width=3)
s.addPath([(3, "a", "c")], start=3, end=3, color="red", depth=48, width=3)

s.addTimeLine(ticks=1)
