\documentclass[a4paper, 10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\ifdefined\nonotes
\usepackage[disable,colorinlistoftodos,textsize=footnotesize]{todonotes}
\else
\usepackage[colorinlistoftodos,textsize=footnotesize]{todonotes}
\fi
\usepackage{graphicx}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage{latexsym}
\usepackage{dsfont}
\usepackage{algpseudocode}

\begin{document}

\input{algorithm}

\section{Definition}

Stream graph $S=(T,V,W,E)$, $W\subseteq T\times V$, $E\subseteq T\times V\otimes V$, {\em i.e.} $(t,uv)\in E$.

In a stream graph $S$, a path $P$ from $(\alpha,u)$ to $(\omega,v)$ is a sequence $(t_0, u_0, v_0), (t_1, u_1, v_1), \dots, (t_k, u_k, v_k)$ of elements of $T\times V \times V$ such that $u_0 = u$, $v_k=v$, $t_0 \geq \alpha$, $t_k \leq \omega$, for all $i$, $t_i \leq t_{i+1}$, $v_i = u_{i+1}$, and $(t_i, u_iv_i)\in E$, $[\alpha, t_0]\times \{u\} \subseteq W$, $[t_k, \omega]\times \{v\}\subseteq W$, and for all $i$, $[t_i, t_{i+1}]\times \{v_i\} \subseteq W$.

We say $P$ has length $k+1$, and duration $t_k - t_0$.
Given a $(\alpha, u)$ and a node $v$, let us consider $\omega_0$ the smallest time such that $(\alpha, u)$ reaches $(\omega_0, v)$. Then say $(\alpha,u)$ has a time to reach of $\omega_0 - \alpha$.

$P$ is a shortest path from $(\alpha, u)$ to $(\omega, v)$ if it is has minimal length; $P$ is a fastest path from $(\alpha, u)$ to $(\omega, v)$ if it has minimal duration; $P$ is a foremost path from $(\alpha, u)$ to $(\omega, v)$ if it has minimal time to reach.  


$$
    {\cal B}(t,v) = \sum_{u,w}\int_{i,j} \frac{\sigma((i,u),(j,w),(t,v))}{\sigma((i,u),(j,w))}didj
$$

This definition can easily be extended to any node cluster $X\subseteq W$:

$$
    {\cal B}(X) = \sum_{u,w} \int_{i,j} \frac{\sigma((i,u), (j,w), X)}{\sigma((i,u),(j,w))}
$$


\section{Data}

Data with existing ground truth.

Email traces with thread information;

\section{Related work}

The majority of works working on extensions of betweenness centrality either compute a centrality for dynamic graphs~\cite{}, or extend centrality to temporal networks~\cite{}.

Albeit interesting, the dynamic graph representation is not always adequate, especially in the case where the time resolution is high, or even continuous; for instance, if one were to freeze time and study the graph of who is sending emails to who right now, it is likely than the graph will be empty, or at least very disconnected~\footnote{On the contrary, some data is especially well suited to a dynamic graph representation: the web graph is an example.}.
For this reason, we will not consider extensions of centrality on dynamic graphs: they are interesting, but are a different research problem.

On temporal networks, the first extensions of centrality are made by~\cite{pan2011path}, however the authors only consider closeness centrality.

Moreover, as~\cite{takaguchi2016coverage} noted, most approaches suffer from two limitations:
\begin{enumerate}
    \item Parameter dependent;
    \item Computationnally inefficient.
\end{enumerate}

The authors in~\cite{takaguchi2016coverage} devise two notions, Temporal Coverage Centrality and Temporal Boundary Coverage Centrality, defined respectively for a node $v$ as the fraction of pairs of nodes such that there is at least one fastest path going through $v$, and the fraction of pairs of nodes such that there is exactly one fastest path going through $v$

In addition to these limitations, let us note that to the best of our knowledge, all approaches in the current literature are time-biaised in the case of a time-bounded stream; typically, one can expect middle times to be more central than early and late times, which is a measurement artifact: all paths, even starting at the beginning, will likely be active in the middle of $T$, and paths starting after $\frac{T}{2}$ have an increasing chance of not ending before $\omega$.

Most of the time, time-based centralities focus on foremost paths. However, if we consider the existing graph theoretical intuition, working on foremost paths is more akin to closeness centrality: the extent at which, going through $u$, one can reach early many other nodes.

Foremost paths enumeration has already been studied extensively in the literature~\cite{pan2011path}. On the other hand, finding all shortest fastest paths is harder. Contrary to foremost paths which can be optimized locally, and can be computed by adapting a breadth-first search algorithm, shortest fastest paths are global.~\cite{wu2016efficient} devises algorithms to compute the duration of the fastest paths from one node to every other. Their assumption is that a fastest path is also a foremost path from its first link to the destination. The authors then consider all $t\in T$ as starting points, and repeat a foremost path type search at every $t$. Notice however that they only store the duration of the fastest path, not the path itself, and assumes discrete time; moreover, it assumes fastest paths are fastest paths from $(\alpha, u)$ to $(\omega, v)$, without considering sub-intervals. It is not readily extendable to compute betweenness centrality.


\section{Motivation and limitations}

The definition does not work well when mixing links with and without durations.

We focus for now on stream graphs with only punctual links, {\em i.e.} $E\subseteq T\times V \otimes V$.

Temporal extensions of centrality can easily be time-biaised (see related work section). To limit the impact of the time bounds, all starting and ending points are considered. This is based on the intuition that very central nodes should connect many other nodes at any time, not just once in the stream. 

%Why shortest fastest paths?

This definition is the first generalization, in the formal sense, of betweenness centrality.
Indeed, in a graph-equivalent stream, the betweenness centrality of any node $v$ is equal to $\frac{|T|^2}{2}$ the betweenness of $v$ in the induced graph.

\section{Algorithm scheme}

Notice that saying that $p$ is a shortest fastest path from $(i,u)$ to $(j,v)$, $u,v\in V$, $i,j\in T$ means that there exists no $p'$ from $(i,u)$ to $(j,v)$ such that $\bar{p'} < \bar{p}$.

In other words, give $u,v$, once one has found all existing shortest fastest paths of duration $k$, there can only exist shortest fastest paths of duration $k'>k$. 

Notice that fastest paths do not form a partition of $T^2$ (see example 1).

\todo[inline]{Define "family" of sfp: $p$ and $p'$ are part of the same family $=>$ they involve the same links, and there are sfp of the same form in a continous interval, kind of. Not necessary for $(t,uv)$ streams.}

Given $u,v\in V$, for all $i,j\in T$, let us find all shortest fastest paths from $(i,u)$ to $(j,v)$.

We denote by $t_x(u)$ the time of the $x^{th}$ link of $u$, for $x=0..k$. Let us start by noting that:
\begin{itemize}
    \item $\forall j > i, (i,u)\not\leadsto (j,v)$,
    \item $\forall j, \forall i > t_k(u), (i,u) \not\leadsto (j,v)$,
    \item $\forall i, \forall j < t_0(u), (i,u) \not\leadsto (j,v)$,
\end{itemize}

Similarly:
\begin{itemize}
    \item $\forall j, \forall i > t_k(v), (i,u\not\leadsto (j,v)$,
    \item $\forall i, \forall j < t_0(v), (i,u) \not\leadsto (j,v)$,
\end{itemize}

Moreover, any shortest fastest path from $(i,u)$ to $(j,v)$ must start with a link $(t,ux), t\in T, x,\in V\setminus\{u\}$.

At any time, we aim at finding all sfp of duration $k$. Notice that duration can be optimized greedily (a path cannot {\em reduce} duration as links are added). As such, any path we find having duration $k'>k$ is left for later consideration.

At any time we take the current shortest fastest path we know, and attempt to extend it.

For all link $(t,xy)\in E$, there exists at most one sfp $p$ from $(i,u)$ to $(j,v)$ such that $(t,xy)\in p$. This is true in link streams, but not in stream graphs.


\subsection{Notations}

$$
    \tau(u) = \{t: x\in V, \exists (t,ux)\in E \}
$$
$$
    \tau(v) = \{t: x\in V, \exists (t,vx)\in E \}
$$

The first possible $j$ such that $\exists (i,u)\leadsto (j,v)$ is:
$$
    i_0 = \min(\{t: \exists (t,xy)\in E, x\in \{u,v\}, y\in V\})
$$
$$
    j_k = \max(\{t: \exists (t,xy)\in E, x\in \{u,v\}, y\in V\})
$$

\section{Perspectives}

Extension to non-punctual stream graphs

\bibliographystyle{plain}
\bibliography{main}

\newpage

\section{Examples}
%% 7 examples
\subsection{Simple path of length $1$, duration $0$}
\includegraphics{img/toy1}
\hfill
\includegraphics[width=0.3\linewidth]{img/i-j-1}

\subsection{Simple path of length $2$, duration $>0$}
\includegraphics{img/toy2}
\hfill
\includegraphics[width=0.3\linewidth]{img/i-j-2}

\subsection{Multiple identical shortest fastest paths}
\includegraphics{img/toy3}
\hfill
\includegraphics[width=0.3\linewidth]{img/i-j-3}

\subsection{Paths of varying lengths and durations}
\includegraphics{img/toy4}
\hfill
\includegraphics[width=0.3\linewidth]{img/i-j-4}

\subsection{Fastest path is not earliest}
\includegraphics{img/toy5}
\hfill
\includegraphics[width=0.3\linewidth]{img/i-j-5}

\subsection{Fastest path is not the earliest}
\includegraphics{img/toy6}
\hfill
\includegraphics[width=0.3\linewidth]{img/i-j-6}
%\includegraphics[width=0.45\linewidth]{img/toy7}

\end{document}
