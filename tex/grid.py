import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

alpha = 0
omega = 10
tau_u = [1, 3, 4, 8]
tau_v = [2, 4, 5]

fig = plt.figure()
ax = fig.add_subplot(111)

ax.xaxis.set_ticks([alpha] + tau_u + [omega])
ax.yaxis.set_ticks([alpha] + tau_v + [omega])
ax.grid(linestyle='--')
ax.set_xlabel('i')
ax.set_ylabel('j')

# Path linking i <= 1 to j >= 5
ax.add_patch(Rectangle((0,5), 1, 5))
ax.add_patch(Rectangle((0,0), omega-alpha, min(tau_v), fill='black'))
ax.add_patch(Rectangle((max(tau_u), 0), omega-max(tau_u), omega - alpha, fill='black'))


plt.show()
